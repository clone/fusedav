#ifndef foostatcachehfoo
#define foostatcachehfoo

/***
  Copyright (c) 2004-2006 Lennart Poettering

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
***/

#include <sys/stat.h>

int stat_cache_get(const char *fn, struct stat *st);
void stat_cache_set(const char *fn, const struct stat *st);
void stat_cache_invalidate(const char*fn);

void dir_cache_invalidate(const char*fn);
void dir_cache_invalidate_parent(const char *fn);
void dir_cache_begin(const char *fn);
void dir_cache_finish(const char *fn, int success);
void dir_cache_add(const char *fn, const char *subdir);
int dir_cache_enumerate(const char *fn, void (*f) (const char*fn, const char *subdir, void *user), void *user);

void cache_free(void);
void cache_alloc(void);

#endif
